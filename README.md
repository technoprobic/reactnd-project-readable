# Udacity Readable Project
This is the Readable project for Udacity ReactJS certification. It is written using the ReactJS framework on top of NodeJS. Udacity provides the [backend server](https://github.com/udacity/reactnd-project-readable-starter) for use with this front end.

On the front end, categories of blog-like posts are shown. The user can create, edit, delete, and upvote/downvote the posts. A user can also comment on each of the posts.

## Setup/Install/Use
To install, you should have NodeJS already installed on the system. Download the above server repository to a directory and change to that directory. To complete set up, run `npm install`. To use, run `node server`. For this front end, download/clone this repository to a directory and change to that directory. To complete set up, run `npm install`. To use, run `npm start`.

## create-react-app
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). You can find more information on how to perform common tasks [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
