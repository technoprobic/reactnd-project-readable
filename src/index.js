import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import promise from "redux-promise";

import reducers from "./reducers";
import PostsIndex from "./components/posts_index";
import PostsNew from "./components/posts_new";
import PostsShow from "./components/posts_show";
import PostsEdit from "./components/posts_edit";

import CommentsIndex from "./components/comments_index";
import CommentsNew from "./components/comments_new";
import CommentsShow from "./components/comments_show";
import CommentsEdit from "./components/comments_edit";

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/posts/new" component={PostsNew} />
          <Route path="/posts/edit/:id" component={PostsEdit} />
          <Route path="/posts/category/:id" component={PostsIndex} />
          <Route path="/posts/:id" component={PostsShow} />
          <Route path="/comments/new" component={CommentsNew} />
          <Route path="/comments/edit/:id" component={CommentsEdit} />
          <Route path="/comments/:id" component={CommentsShow} />
          <Route path="/" component={PostsIndex} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>,
  document.querySelector(".container")
);
