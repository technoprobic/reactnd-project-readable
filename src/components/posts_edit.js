import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchPost, submitEditPost } from "../actions";

class PostsEdit extends Component {

  constructor(props) {
		super(props);

		//this.renderCategoryOptions = this.renderCategoryOptions.bind(this);
	}

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
    if (this.props.initialValues) {
      this.props.change('title', this.props.initialValues.title);
      this.props.change('body', this.props.initialValues.body);
    }
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    this.props.submitEditPost(this.props.match.params.id ,values, () => {
      this.props.history.push(`/posts/${this.props.match.params.id}`);
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <Field
          label="Title"
          name="title"
          component={this.renderField}
        />
        <Field
          label="Post Body"
          name="body"
          component={this.renderField}
        />
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to={`/posts/${this.props.match.params.id}`} className="btn btn-danger">Cancel</Link>
      </form>
    );
  }
}

function validate(values) {
  // console.log(values)
  const errors = {};

  // Validate the inputs from 'values'
  if (!values.title) {
    errors.title = "Enter a title";
  }
  if (!values.body) {
    errors.body = "Enter some content in the body";
  }

  return errors;
}

function mapStateToProps({ posts }, ownProps) {
  return {
    initialValues: posts[ownProps.match.params.id]
  };
}

export default reduxForm({
  validate,
  form: "PostsEditForm",
  enableReinitialize : true
})(connect(mapStateToProps, { fetchPost, submitEditPost })(PostsEdit));
