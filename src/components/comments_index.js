import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchComments, upvoteComment, downvoteComment } from "../actions";
import _ from "lodash";

class CommentsIndex extends Component {

  constructor(props) {
    super(props);

    this.state = {
      sortKey: 'voteScore',
      sortAscDesc: 'desc'
    };

    this.setSortDateAsc = this.setSortDateAsc.bind(this);
    this.setSortDateDesc = this.setSortDateDesc.bind(this);
    this.setSortVoteScoreAsc = this.setSortVoteScoreAsc.bind(this);
    this.setSortVoteScoreDesc = this.setSortVoteScoreDesc.bind(this);
    this.onUpvoteClick = this.onUpvoteClick.bind(this);
    this.onDownvoteClick = this.onDownvoteClick.bind(this);

  }

  componentDidMount() {
    this.props.fetchComments(this.props.postID);
  }

  setSortDateAsc() {

    this.setState({
      sortKey: 'timestamp',
      sortAscDesc: 'asc'
    });
  }

  setSortDateDesc() {

    this.setState({
      sortKey: 'timestamp',
      sortAscDesc: 'desc'
    });
  }

  setSortVoteScoreAsc() {

    this.setState({
      sortKey: 'voteScore',
      sortAscDesc: 'asc'
    });
  }

  setSortVoteScoreDesc() {

    this.setState({
      sortKey: 'voteScore',
      sortAscDesc: 'desc'
    });
  }

  onUpvoteClick(id) {

    this.props.upvoteComment(id, () => {
      this.props.fetchComments(this.props.postID);
    });
  }

  onDownvoteClick(id) {

    this.props.downvoteComment(id, () => {
      this.props.fetchComments(this.props.postID);
    });
  }

  renderComments() {
      return _.map( _.orderBy(this.props.comments, this.state.sortKey, this.state.sortAscDesc), comment => {
        return (
          <tr key={comment.id}>
            <td>
              <Link to={`/comments/${comment.id}`}>
                {comment.body}
              </Link>
            </td>
            <td>
              {new Date(comment.timestamp).toString().substr(0,24)}
            </td>
            <td>
              {comment.voteScore} <span onClick={this.onUpvoteClick.bind(this, comment.id)} className="glyphicon glyphicon-thumbs-up"></span><span onClick={this.onDownvoteClick.bind(this, comment.id)} className="glyphicon glyphicon-thumbs-down"></span>
            </td>
        </tr>
        );
      });

  }

  render() {
    return (
      <div>
        <h3>Comments ({_.size(this.props.comments) })</h3>
        <table className = "table table-hover">
          <thead>
            <tr><th>Comment</th><th>Date <span onClick={this.setSortDateDesc } className="glyphicon glyphicon-arrow-up"></span><span onClick={this.setSortDateAsc } className="glyphicon glyphicon-arrow-down"></span></th><th>Vote Score <span onClick={this.setSortVoteScoreDesc } className="glyphicon glyphicon-arrow-up"></span><span onClick={this.setSortVoteScoreAsc } className="glyphicon glyphicon-arrow-down"></span></th></tr>
          </thead>
          <tbody>
            {this.renderComments()}
          </tbody>
        </table>
      </div>
    );

  }
}

function mapStateToProps(state) {
  return {
    comments: state.comments
  };
}

export default connect(mapStateToProps, { fetchComments, upvoteComment, downvoteComment })(CommentsIndex);
