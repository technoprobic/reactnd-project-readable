import React, { Component } from "react";
import _ from "lodash";
import axios from "axios";
import { ROOT_URL, AUTH_HEADER } from "../actions";
import PropTypes from 'prop-types';

export default class PostCommentCount extends Component {

  constructor(props) {
    super(props);

    this.state = {
      postCommentCount: 0
    };

    this.getPostCommentCount = this.getPostCommentCount.bind(this);

  }

  componentDidMount() {

    console.log('postID for count: ', this.props.postID);
    this.getPostCommentCount(this.props.postID);

  }

  getPostCommentCount(postID) {
    //console.log(postID);
    let commentCount = 0;
    const comments = axios
    .get(`${ROOT_URL}/posts/${postID}/comments`, {headers: AUTH_HEADER})
    .then( resp => {
      console.log ('getCommentCount resp: ', resp.data );
      console.log ('getCommentCount resp size: ', resp.data.length );
      commentCount = resp.data.length;
      this.setState({
        postCommentCount: commentCount
      });
    });
  }


  render() {
    return (
      <span>{this.state.postCommentCount}</span>
    );

  }
}

PostCommentCount.propTypes = {
  postID: PropTypes.string.isRequired,
}
