import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchPost, deletePost, upvotePost, downvotePost } from "../actions";
import CommentsIndex from "./comments_index";

class PostsShow extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  onDeleteClick() {
    const { id } = this.props.match.params;

    this.props.deletePost(id, () => {
      this.props.history.push("/");
    });
  }

  onUpvoteClick() {
    const { id } = this.props.match.params;

    this.props.upvotePost(id, () => {
      this.props.fetchPost(id);
    });
  }

  onDownvoteClick() {
    const { id } = this.props.match.params;

    this.props.downvotePost(id, () => {
      this.props.fetchPost(id);
    });
  }

  render() {
    const { post } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Link to="/">Back To Index</Link>

        <h3>{post.title}</h3>
        <h6>Category: {post.category}</h6>
        <h6>Author: {post.author}</h6>
        <h6>Time: {new Date(post.timestamp).toString().substr(0,24)}</h6>
        <p>{post.body}</p>
        <p>Vote Score: {post.voteScore} <span onClick={this.onUpvoteClick.bind(this)} className="glyphicon glyphicon-thumbs-up"></span><span onClick={this.onDownvoteClick.bind(this)} className="glyphicon glyphicon-thumbs-down"></span></p>
        <div>
          <Link to={`/posts/edit/${this.props.match.params.id}`} className="btn btn-primary">Edit</Link>
          <button
            className="btn btn-danger"
            onClick={this.onDeleteClick.bind(this)}
          >
            Delete Post
          </button>
        </div>
        <div>
          <div>
            <CommentsIndex postID={this.props.match.params.id} />
          </div>
          <Link to={`/comments/new/${this.props.match.params.id}`} className="btn btn-primary">Add Comment</Link>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ posts }, ownProps) {
  return { post: posts[ownProps.match.params.id] };
}

export default connect(mapStateToProps, { fetchPost, deletePost, upvotePost, downvotePost })(PostsShow);
