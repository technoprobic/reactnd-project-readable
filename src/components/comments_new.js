import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createComment} from "../actions";

class CommentsNew extends Component {

  constructor(props) {
		super(props);

	}

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }

  onSubmit(values) {
    values.id = Date.now();
    values.timestamp = Date.now();
    values.voteScore = 1;
    values.parentId = this.props.location.pathname.split('/').pop(-1);
    this.props.createComment(values, () => {
      this.props.history.push(`/posts/${this.props.location.pathname.split('/').pop(-1)}`);
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <Field
          label="Author"
          name="author"
          component={this.renderField}
        />
        <Field
          label="Comment Body"
          name="body"
          component={this.renderField}
        />
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to={`/posts/${this.props.location.pathname.split('/').pop(-1)}`} className="btn btn-danger">Cancel</Link>
      </form>
    );
  }
}

function validate(values) {
  // console.log(values)
  const errors = {};

  // Validate the inputs from 'values'
  if (!values.author) {
    errors.author = "Enter an author";
  }
  if (!values.body) {
    errors.body = "Enter some content in the body";
  }

  return errors;
}

function mapStateToProps(state) {
  return {  };
}

export default reduxForm({
  validate,
  form: "CommentsNewForm"
})(connect(mapStateToProps, { createComment })(CommentsNew));
