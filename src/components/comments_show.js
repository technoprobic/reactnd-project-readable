import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchComment, deleteComment, upvoteComment, downvoteComment } from "../actions";

class CommentsShow extends Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchComment(id);
  }

  onDeleteClick() {
    const { id } = this.props.match.params;
    const parentId = this.props.comment.parentId;

    this.props.deleteComment(id, () => {
      this.props.history.push(`/posts/${parentId}`);
    });
  }

  onUpvoteClick() {
    const { id } = this.props.match.params;

    this.props.upvoteComment(id, () => {
      this.props.fetchComment(id);
    });
  }

  onDownvoteClick() {
    const { id } = this.props.match.params;

    this.props.downvoteComment(id, () => {
      this.props.fetchComment(id);
    });
  }

  render() {
    const { comment } = this.props;

    if (!comment) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Link to={`/posts/${comment.parentId}`} >Back To Post</Link>

        <h6>Author: {comment.author}</h6>
        <h6>Time: {new Date(comment.timestamp).toString().substr(0,24)}</h6>
        <p>{comment.body}</p>
        <p>Vote Score: {comment.voteScore} <span onClick={this.onUpvoteClick.bind(this)} className="glyphicon glyphicon-thumbs-up"></span><span onClick={this.onDownvoteClick.bind(this)} className="glyphicon glyphicon-thumbs-down"></span></p>
        <div>
          <Link to={`/comments/edit/${this.props.match.params.id}`} className="btn btn-primary">Edit</Link>
          <button
            className="btn btn-danger"
            onClick={this.onDeleteClick.bind(this)}
          >
            Delete Comment
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ comments }, ownProps) {
  return { comment: comments[ownProps.match.params.id] };
}

export default connect(mapStateToProps, { fetchComment, deleteComment, upvoteComment, downvoteComment })(CommentsShow);
