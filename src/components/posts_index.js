import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchPosts, fetchCategories, upvotePost, downvotePost, deletePost } from "../actions";
import _ from "lodash";
import axios from "axios";
import { ROOT_URL, AUTH_HEADER } from "../actions";
import PostCommentCount from "./post_comment_count";

class PostsIndex extends Component {

  constructor(props) {
    super(props);

    this.state = {
      sortKey: 'voteScore',
      sortAscDesc: 'desc',
      postCommentCounts: {}
    };

    this.setSortDateAsc = this.setSortDateAsc.bind(this);
    this.setSortDateDesc = this.setSortDateDesc.bind(this);
    this.setSortVoteScoreAsc = this.setSortVoteScoreAsc.bind(this);
    this.setSortVoteScoreDesc = this.setSortVoteScoreDesc.bind(this);
    this.onUpvoteClick = this.onUpvoteClick.bind(this);
    this.onDownvoteClick = this.onDownvoteClick.bind(this);

  }

  componentDidMount() {

    this.props.fetchPosts();
    this.props.fetchCategories();

  }

  renderCategoryLinks() {

    const { categories } = this.props;

    if (!categories) {
      return <Link to="/" className="btn btn-primary">Home</Link>;
    }

    return _.map( this.props.categories, category => {
      return (
        <Link key={category.path} className="btn btn-primary" to={`/category/${category.path}`}>{category.name}</Link>
      );
    });
  }

  renderPostsOld() {
    return _.map( _.sortBy(this.props.posts, this.state.sortKey, this.state.sortAscDesc) , post => {
      return (
        <li className="list-group-item" key={post.id}>
          <Link to={`/posts/${post.id}`}>
            {post.title}
          </Link>
        </li>
      );
    });
  }

  /*setSort(newSortKey, newSortAscDesc) {

    this.setState({
      sortKey: newSortKey,
      sortAscDesc: newSortAscDesc
    });
  } */

  setSortDateAsc() {

    this.setState({
      sortKey: 'timestamp',
      sortAscDesc: 'asc'
    });
  }

  setSortDateDesc() {

    this.setState({
      sortKey: 'timestamp',
      sortAscDesc: 'desc'
    });
  }

  setSortVoteScoreAsc() {

    this.setState({
      sortKey: 'voteScore',
      sortAscDesc: 'asc'
    });
  }

  setSortVoteScoreDesc() {

    this.setState({
      sortKey: 'voteScore',
      sortAscDesc: 'desc'
    });
  }

  onUpvoteClick(id) {

    this.props.upvotePost(id, () => {
      this.props.fetchPosts();
    });
  }

  onDownvoteClick(id) {

    this.props.downvotePost(id, () => {
      this.props.fetchPosts();
    });
  }

  onDeleteClick(id) {

    this.props.deletePost(id, () => {
      this.props.history.push("/");
    });
  }

  renderPosts() {

    if (this.props.location.pathname === '/') {
      return _.map( _.orderBy(this.props.posts, this.state.sortKey, this.state.sortAscDesc), post => {
        return (
          <tr key={post.id}>
            <td>
              <Link to={`/posts/${post.id}`}>
                {post.title}
              </Link>
              <div>
                <span
                  className="btn btn-xs">
                <Link to={`/posts/edit/${post.id}`} >Edit Post</Link>
                </span>
                <span
                  className="btn btn-xs"
                  onClick={this.onDeleteClick.bind(this, post.id)}
                >
                  Delete Post
                </span>
              </div>
            </td>
            <td>
              {post.author}
            </td>
            <td>
              {new Date(post.timestamp).toString().substr(0,24)}
            </td>
            <td>
              {post.voteScore} <span onClick={this.onUpvoteClick.bind(this, post.id)} className="glyphicon glyphicon-thumbs-up"></span><span onClick={this.onDownvoteClick.bind(this, post.id)} className="glyphicon glyphicon-thumbs-down"></span>
            </td>
            <td>
              <PostCommentCount postID={post.id} />
            </td>
        </tr>
        );
      });

    } else {
      return _.map( _.orderBy( _.filter(this.props.posts, post => post.category === this.props.location.pathname.split('/').pop(-1) ) , this.state.sortKey, this.state.sortAscDesc), post => {
        return (
          <tr key={post.id}>
            <td>
              <Link to={`/posts/${post.id}`}>
                {post.title}
              </Link>
              <div>
                <span
                  className="btn btn-xs">
                <Link to={`/posts/edit/${post.id}`} >Edit Post</Link>
                </span>
                <span
                  className="btn btn-xs"
                  onClick={this.onDeleteClick.bind(this, post.id)}
                >
                  Delete Post
                </span>
              </div>
            </td>
            <td>
              {post.author}
            </td>
            <td>
              {new Date(post.timestamp).toString().substr(0,24)}
            </td>
            <td>
              {post.voteScore} <span onClick={this.onUpvoteClick.bind(this, post.id)} className="glyphicon glyphicon-thumbs-up"></span><span onClick={this.onDownvoteClick.bind(this, post.id)} className="glyphicon glyphicon-thumbs-down"></span>
            </td>
            <td>
              <PostCommentCount postID={post.id} />
            </td>

        </tr>
        );
      });
    }

  }

  render() {
    return (
      <div>
        <h3>Post Categories</h3>
        <div>
          <Link className="btn btn-primary" to="/">all</Link>{this.renderCategoryLinks()}
        </div>
        <h3>Posts</h3>
        <table className = "table table-hover">
          <thead>
            <tr><th>Title</th><th>Author</th><th>Date <span onClick={this.setSortDateDesc } className="glyphicon glyphicon-arrow-up"></span><span onClick={this.setSortDateAsc } className="glyphicon glyphicon-arrow-down"></span></th><th>Vote Score <span onClick={this.setSortVoteScoreDesc } className="glyphicon glyphicon-arrow-up"></span><span onClick={this.setSortVoteScoreAsc } className="glyphicon glyphicon-arrow-down"></span></th><th>Comments</th></tr>
          </thead>
          <tbody>
            {this.renderPosts()}
          </tbody>
        </table>
        <div className="text-xs-right">
          <Link className="btn btn-primary" to="/posts/new">
            Add a Post
          </Link>
        </div>
      </div>
    );

  }
}

function mapStateToProps(state) {
  return {
    posts: state.posts,
    categories: state.categories
  };
}

export default connect(mapStateToProps, { fetchPosts, fetchCategories, upvotePost, downvotePost, deletePost })(PostsIndex);
