import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createPost, fetchCategories } from "../actions";
import _ from "lodash";

class PostsNew extends Component {

  constructor(props) {
		super(props);

		//this.renderCategoryOptions = this.renderCategoryOptions.bind(this);
	}

  componentDidMount() {
    this.props.fetchCategories();
  }

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <input className="form-control" type="text" {...field.input} />
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }

  renderSelectField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? "has-danger" : ""}`;

    return (
      <div className={className}>
        <label>{field.label}</label>
        <select className="form-control" type="text" {...field.input}>
          {field.categoryOptions}
        </select>
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }

  renderCategoryOptions() {

    const { categories } = this.props;

    if (!categories) {
      return <option>None</option>;
    }

    let categoryOptions = _.map( this.props.categories, category => {
      //console.log('category map: ', category.path, category.name);
      return (
        <option key={category.path} value={category.path} >{category.name}</option>
      );
    });
    categoryOptions.unshift(<option key="none"></option>);
    //console.log('categoryOptions: ', categoryOptions);
    return categoryOptions;
  }

  onSubmit(values) {
    values.id = Date.now();
    values.timestamp = Date.now();
    values.voteScore = 0;
    this.props.createPost(values, () => {
      this.props.history.push("/");
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <Field
          label="Title"
          name="title"
          component={this.renderField}
        />
        <Field
          label="Category"
          name="category"
          categoryOptions = {this.renderCategoryOptions()}
          component={this.renderSelectField}
        />
        <Field
          label="Author"
          name="author"
          component={this.renderField}
        />
        <Field
          label="Post Body"
          name="body"
          component={this.renderField}
        />
        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to="/" className="btn btn-danger">Cancel</Link>
      </form>
    );
  }
}

function validate(values) {
  // console.log(values)
  const errors = {};

  // Validate the inputs from 'values'
  if (!values.title) {
    errors.title = "Enter a title";
  }
  if (!values.category) {
    errors.category = "Select a category";
  }
  if (!values.author) {
    errors.author = "Enter an author";
  }
  if (!values.body) {
    errors.body = "Enter some content in the body";
  }

  return errors;
}

function mapStateToProps(state) {
  return { categories: state.categories };
}

export default reduxForm({
  validate,
  form: "PostsNewForm"
})(connect(mapStateToProps, { createPost, fetchCategories })(PostsNew));
