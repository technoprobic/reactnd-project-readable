import axios from "axios";

export const FETCH_POSTS = "fetch_posts";
export const FETCH_POST = "fetch_post";
export const FETCH_CATEGORIES = "fetch_categories";
export const CREATE_POST = "create_post";
export const SUBMIT_EDIT_POST = "submit_edit_post";
export const DELETE_POST = "delete_post";
export const UPVOTE_POST = "upvote_post";
export const DOWNVOTE_POST = "downvote_post";

export const FETCH_COMMENTS = "fetch_comments";
export const FETCH_COMMENT = "fetch_comment";
export const CREATE_COMMENT = "create_comment";
export const SUBMIT_EDIT_COMMENT = "submit_edit_comment";
export const DELETE_COMMENT = "delete_comment";
export const UPVOTE_COMMENT = "upvote_comment";
export const DOWNVOTE_COMMENT = "downvote_comment";

export const ROOT_URL = "http://localhost:3001";
export const AUTH_HEADER = { 'Authorization': 'whatever-you-want-123456789-987654321' };

export function fetchPosts() {
  const request = axios
    .get(`${ROOT_URL}/posts`, {headers: AUTH_HEADER});

  return {
    type: FETCH_POSTS,
    payload: request
  };
}

export function fetchCategories() {
  const request = axios
  .get(`${ROOT_URL}/categories`, {headers: AUTH_HEADER});

  return {
    type: FETCH_CATEGORIES,
    payload: request
  };
}

export function createPost(values, callback) {
  const request = axios
    .post(`${ROOT_URL}/posts`, values, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: CREATE_POST,
    payload: request
  };
}

export function submitEditPost(id, values, callback) {
  const request = axios
    .put(`${ROOT_URL}/posts/${id}`, values, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: CREATE_POST,
    payload: request
  };
}

export function fetchPost(id) {
  const request = axios.get(`${ROOT_URL}/posts/${id}`, {headers: AUTH_HEADER});

  return {
    type: FETCH_POST,
    payload: request
  };
}

export function upvotePost(id, callback) {
  const request = axios
    .post(`${ROOT_URL}/posts/${id}`, {option: "upVote"}, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: UPVOTE_POST,
    payload: id
  };
}

export function downvotePost(id, callback) {
  const request = axios
    .post(`${ROOT_URL}/posts/${id}`, {option: "downVote"}, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: DOWNVOTE_POST,
    payload: id
  };
}

export function deletePost(id, callback) {
  const request = axios
    .delete(`${ROOT_URL}/posts/${id}`, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: DELETE_POST,
    payload: id
  };
}

// comments section
export function fetchComments(postID, callback) {
  const request = axios.get(`${ROOT_URL}/posts/${postID}/comments`, {headers: AUTH_HEADER});

  return {
    type: FETCH_COMMENTS,
    payload: request
  };
}

export function createComment(values, callback) {
  const request = axios
    .post(`${ROOT_URL}/comments`, values, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: CREATE_COMMENT,
    payload: request
  };
}

export function submitEditComment(commentID, values, callback) {
  const request = axios
    .put(`${ROOT_URL}/comments/${commentID}`, values, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: CREATE_COMMENT,
    payload: request
  };
}

export function fetchComment(commentID) {
  const request = axios.get(`${ROOT_URL}/comments/${commentID}`, {headers: AUTH_HEADER});

  return {
    type: FETCH_COMMENT,
    payload: request
  };
}

export function upvoteComment(commentID, callback) {
  const request = axios
    .post(`${ROOT_URL}/comments/${commentID}`, {option: "upVote"}, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: UPVOTE_COMMENT,
    payload: commentID
  };
}

export function downvoteComment(commentID, callback) {
  const request = axios
    .post(`${ROOT_URL}/comments/${commentID}`, {option: "downVote"}, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: DOWNVOTE_COMMENT,
    payload: commentID
  };
}

export function deleteComment(commentID, callback) {
  const request = axios
    .delete(`${ROOT_URL}/comments/${commentID}`, {headers: AUTH_HEADER})
    .then(() => callback());

  return {
    type: DELETE_COMMENT,
    payload: commentID
  };
}
