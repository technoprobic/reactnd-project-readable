import _ from "lodash";
import { FETCH_COMMENTS, FETCH_COMMENT, DELETE_COMMENT } from "../actions";

export default function(state = {}, action) {
  switch (action.type) {
    case DELETE_COMMENT:
      return _.omit(state, action.payload);
    case FETCH_COMMENT:
      return { ...state, [action.payload.data.id]: action.payload.data };
    case FETCH_COMMENTS:
      return _.mapKeys(action.payload.data, "id");
    default:
      return state;
  }
}
